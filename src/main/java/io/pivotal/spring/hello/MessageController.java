package io.pivotal.spring.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
	
	private LocalMessageTransformer messageTransformer;
	 
	private RemoteMessageTransformer remoteMessageTransformer;
	
	@Autowired
	public MessageController(LocalMessageTransformer messageTransformer, RemoteMessageTransformer remoteMessageTransformer) {
		this.messageTransformer = messageTransformer;
		this.remoteMessageTransformer = remoteMessageTransformer;
	}
	

	/*
	@Autowired
	public MessageController(LocalMessageTransformer messageTransformer) {
		this.messageTransformer = messageTransformer;
	}
	*/
	@RequestMapping(value = "/transform", method = RequestMethod.GET) 
	public String transform(@RequestParam String message) {
		return messageTransformer.transform(message) +"\n";
	}
	
	@RequestMapping("/")
	public String helloWorld() {
		return "Hello World!\n";
	}
	
	@RequestMapping("/{name}")
	public String helloName(@PathVariable("name") String name) {
		return "Hello "+name+"!\n";
	}
	
	
	@RequestMapping(value = "/remoteTransform", method = RequestMethod.GET)
	public String remoteTransform(@RequestParam String message) {
		return remoteMessageTransformer.callRemoteTransformMethod(message);
	}
	
	
}
