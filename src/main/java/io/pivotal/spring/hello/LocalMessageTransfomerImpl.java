package io.pivotal.spring.hello;

import org.springframework.stereotype.Service;


@Service
public class LocalMessageTransfomerImpl implements LocalMessageTransformer {

	@Override
	public String transform(String message) {
		return new StringBuilder(message).reverse().toString();
	}

}
