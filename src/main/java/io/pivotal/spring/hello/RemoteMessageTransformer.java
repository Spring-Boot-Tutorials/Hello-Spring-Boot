package io.pivotal.spring.hello;

public interface RemoteMessageTransformer {
	String callRemoteTransformMethod(String source);
}
