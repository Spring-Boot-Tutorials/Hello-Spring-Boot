package io.pivotal.spring.hello;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class RemoteMessageTransformerImpl implements RemoteMessageTransformer {
	

	private static final String TRANSFORM_URL = "http://localhost:8082/transform?message={message}";
	
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public String callRemoteTransformMethod(String message) {
		
		Map<String, String> params = new HashMap<String, String>();
	    
		params.put("message", message);
	    
		String transformedString = restTemplate.getForObject(TRANSFORM_URL, String.class, params);
		
		return transformedString;
	}

}
