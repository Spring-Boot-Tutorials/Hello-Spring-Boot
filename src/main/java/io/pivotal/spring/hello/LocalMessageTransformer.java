package io.pivotal.spring.hello;

public interface LocalMessageTransformer {
	
	String transform(String message);

}
